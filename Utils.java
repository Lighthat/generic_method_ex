package q71;

import java.util.*;

/**
 * Provides a way to sort a Collection of generic Pairs.
 * 
 * @author Christian Aghyarian
 */
public class Utils
{
    public Utils(){}
    
    public static <K extends Comparable<? super K>, V> ArrayList<Pair<K,V>> sortPairCollection(Collection<Pair<K,V>> col)
    {
        ArrayList<Pair<K,V>> list = new ArrayList<>(col);
        Collections.sort(list, new Comparator<Pair<K,V>>(){
            @Override
            public int compare(Pair<K,V> x, Pair<K,V> y) {
                return x.k().compareTo(y.k());
            }
        });
        return list;
    }
   
   
    public static void main(String args[])
    {
        ArrayList<Pair<String, Integer>> list = new ArrayList<>();
        Random rand = new Random();
        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for(int i = 0; i < 100; ++i)
        {
            Pair<String, Integer> pair = new Pair<>(generateString(rand, chars, 20), rand.nextInt(200000000));
            list.add(pair);
        }
        for(int i = 0; i < 100; ++i)
        {
            System.out.println(list.get(i));
        }
        list = sortPairCollection(list);
        
        System.out.println("***** Sorted List: \n\n");
        
        for(int i = 0; i < 100; ++i)
        {
            System.out.println(list.get(i));
        }
        
    }
    
    public static String generateString(Random rng, String characters, int length)
    {
        char[] text = new char[length];
        for (int i = 0; i < length; i++)
        {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }
        return new String(text);
    }
}
