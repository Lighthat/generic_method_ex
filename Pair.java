package q71;

import java.io.*;

/**
 * Stores a key value K, and a regular value V.
 * 
 * @author Christian Aghyarian
 */
public class Pair<K, V> implements Serializable
{
    public Pair(K k, V v)
    {
        this.k = k;
        this.v = v;
    }
    
    public K k() { return k; }
    
    public V v() { return v; }
    
    @Override
    public boolean equals(Object obj)
    {
        if(!(obj instanceof Pair))
        {
            return false;
        }
        
        Pair<?, ?> tempObj = (Pair<?, ?>) obj;
        
        if(tempObj.k().equals(this.k()) && tempObj.v().equals(this.v()))
            return true;
        else
            return false;
            
    }
    
    @Override
    public int hashCode()
    {
        return (k == null ? 0 : k.hashCode()) ^ (v == null ? 0 : v.hashCode());
    }
    
    @Override
    public String toString()
    {
        return "[" + k + "] - " + v;
    }
    
    @Override
    public Object clone()
    {
        return new Pair<K, V>(k, v);
    }
    
    private final K k;
    private final V v;
    
}
