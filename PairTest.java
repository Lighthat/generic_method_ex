package q71;

import java.io.*;
import java.util.*;

/**
 * Write a description of class PairTest here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PairTest
{
    public static void main(String args[])
    {
        Pair<Integer, String> pair1 = new Pair<>(0, "hello");
        Pair<Integer, String> pair2 = new Pair<>(1, "bye");
        Pair<String, String>  pair3 = new Pair<>("two", "afternoon");
        Pair<Integer, String> pair4 = new Pair<>(0, "hello");
        
        System.out.println(pair1.equals(pair2)); //f
        System.out.println(pair2.equals(pair1)); //f
        System.out.println(pair1.equals(pair3)); //f
        System.out.println(pair2.equals(pair3)); //f
        System.out.println(pair3.equals(pair1)); //f 
        System.out.println(pair3.equals(pair2)); //f 
        System.out.println(pair1.equals(pair1)); //t
        System.out.println(pair2.equals(pair2)); //t
        System.out.println(pair3.equals(pair3)); //t
        System.out.println(pair1.equals(pair4)); //t
        System.out.println(pair4.equals(pair1)); //t
        
        Pair<Integer, String> pair5 = (Pair<Integer, String>) pair1.clone();
        
        System.out.println();
        System.out.println(pair5.equals(pair1)); //t
        System.out.println(pair1.equals(pair5)); //t
        
        try
        {
            OutputStream file = new FileOutputStream("test.ser");
            ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(pair1);
            out.close();
            file.close();
        }
        catch(IOException e)
        {
            System.out.println("Could not perform operation.\n" + e);
        }
        
        try
        {
            InputStream file = new FileInputStream("test.ser");
            ObjectInputStream in = new ObjectInputStream(file);            
            Pair<Integer, String> readPair = (Pair<Integer, String>)in.readObject();
            in.close();
            file.close();
            System.out.println();
            System.out.println(pair1.equals(readPair)); //t
        }
        catch(ClassNotFoundException e)
        {
            System.out.println("Could not perform operation.\n" + e);
        }
        catch(IOException e)
        {
            System.out.println("Could not perform operation.\n" + e);
        }
        
        System.out.println(pair1.hashCode()); //equal to pair 4
        System.out.println(pair2.hashCode()); //different
        System.out.println(pair3.hashCode()); //different
        System.out.println(pair4.hashCode()); //equal to pair1
        
    }
}
